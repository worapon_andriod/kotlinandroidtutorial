package buu.worapon.kotlinandroidtutorial

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnTextView = findViewById<Button>(R.id.btnTextView)
        btnTextView.setOnClickListener{
            val intent = Intent(MainActivity@this, TextViewActivity::class.java)
            startActivity(intent)
        }

        val btnAutoCompleteTextView = findViewById<Button>(R.id.btnAutoCompleteTextView)
        btnAutoCompleteTextView.setOnClickListener{
            val intent = Intent(MainActivity@this, AutoCompleteTextViewActivity::class.java)
            startActivity(intent)
        }

        val btnCheckedTextView = findViewById<Button>(R.id.btnCheckedTextView)
        btnCheckedTextView.setOnClickListener{
            val intent = Intent(MainActivity@this, CheckedTextViewActivity::class.java)
            startActivity(intent)
        }


        val btnHorizontalScrollView = findViewById<Button>(R.id.btnHorizontalScrollView)
        btnHorizontalScrollView.setOnClickListener{
            val intent = Intent(MainActivity@this, HorizontalScrollViewActivity::class.java)
            startActivity(intent)
        }


        val btnListView = findViewById<Button>(R.id.btnListView)
        btnListView.setOnClickListener{
            val intent = Intent(MainActivity@this, ListViewActivity::class.java)
            startActivity(intent)
        }


        val btnRadioButton = findViewById<Button>(R.id.btnRadioButton)
        btnRadioButton.setOnClickListener{
            val intent = Intent(MainActivity@this, RadioButtonActivity::class.java)
            startActivity(intent)
        }

    }
}